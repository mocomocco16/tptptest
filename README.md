# Prover

## 使う前に
TPTPSetting.hsの中のTPTP libraryの場所と出力先ディレクトリを適切に指定する

``` haskell
testSetting = TestSetting
              {
                --略
                ,outputFolder = "(ここを変更 1/2)"
                  &= help "証明木htmlの出力先"  
                  &= typDir
                ,tptpdir = "(ここを変更 2/2))"  
                  &= help "TPTPライブラリの場所・import axiomで使う"
                  &= name  "t" &= typDir
                --略
              }
```

## 注意

定期的に[lightblueの最新版](https://bitbucket.org/DaisukeBekki/lightblue/)を引っ張ってきてstack.yamlを更新する必要がある

```yaml
  - git: https://mocomocco16@bitbucket.org/DaisukeBekki/lightblue.git
    commit: (ここを変更)
```

## 使い方例

```sh
-> % stack run -- --help
setting Sample

TPTPTEST [OPTIONS] FILES/DIRS

Common flags:
  -i --isdebug=ITEM      計算中の出力の有無
  -d --depth=INT         後ろ向き推論の際の探索の深さ上限
  -o --outputfolder=DIR  証明木htmlの出力先
  -l --timelimit=INT     推論全体を打ち切る時間(マイクロ秒)
  -t --tptpdir=DIR       TPTPライブラリの場所・import axiomで使う
  -s --timer=ITEM        計算時間の表示の有無
  -? --help              Display help message
  -V --version           Print version information
```

```sh
-> % stack run -- "../Test/test.test" -i True -d 20
0 goal   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト ? : [  u10:u6 ] =>u4 u3
	1 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト ? : [  u10:u6 ] =>u4 u3
	1 eqIntro3   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト ? : [  u10:u6 ] =>u4 u3
	1 piIntro1   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト ? : [  u10:u6 ] =>u4 u3
		2 typecheck :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト [  u10:u6 ] =>u4 u3 : type
		2 found :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト [  u10:u6 ] =>u4 u3 : type
			3 piForm1   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト [  u10:u6 ] =>u4 u3 : type
			3 typecheck :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト u6 : type
			3 found :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト u6 : type
				4 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト ? : type
				4 typecheck :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u4 u3 : type
				4 found :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u4 u3 : type
					5 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
					5 eqIntro3   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
					5 piIntro2   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
					5 sigmaIntroハズレ   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
					5 piElimtypecheck   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u4 u3 : type
						6 typecheck :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u3 : type
						6 found :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u3 : type
							7 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
		2 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : u4 u3
		2 eqIntro3   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : u4 u3
		2 piIntro2   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : u4 u3
		2 sigmaIntroハズレ   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : u4 u3
		2 piElim1[(2,T VAR   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u7 : [  u11:type, u12:u6 ] =>u4 u11 [])]   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : u4 u3
			3 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6, u11:type  ト ? : u6
			3 deduced :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6, u11:type  ト u10 : u6
			3 membership   u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト ? : type
			3 deduced :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u6 : type
		2 deduced :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8, u10:u6  ト u7 u7 u3 : u4 u3
	1 deduced :    u0:T, u1:type, u2:type, u3:type, u4:[  u5:type ] =>type, u6:type, u7:[  u8:type, u9:u6 ] =>u4 u8  ト λx0.(u7 u7 u3) : [  u10:u6 ] =>u4 u3
../Test/test.test\tYES\tTheorem\tYES\t../../output/test_dts.html\tUNKNOWN\t\tfof\t(![p]:(b=>f(p))),(![p]:(b=>f(p)))\t(b=>(f(a)))\t\n
```
