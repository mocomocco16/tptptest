module TPTPInfo where

import qualified Syntaxf as F

import Data.Default (Default(..))

import qualified DTS.DTT as DT
import qualified DTS.Alligator.AlligatorBase as AB

data Language =
 THF -- ^ formulae in typed higher-order form
 | TFF -- ^ formulae in typed first-order form
 | FOF -- ^ formulae in first order form
 | CNF -- ^ formulae in clause normal form.
  deriving(Eq)

instance Show Language where
  show role =
    case role of
     THF -> "thf"
     TFF ->"tff"
     FOF -> "fof"
     CNF -> "cnf"

instance Read Language where
  readsPrec _  str =
    let lans = [THF,TFF,FOF,CNF]
    in
      map (\(ro,(s,r))-> (ro,r))
        $filter (\(ro,(s,r)) -> show ro == s)
          $ zip lans $ map (\lan -> splitAt (length (show lan)) str) lans


data Role =
   Axiom
 | Hypothesis
 | Definition
 | Assumption
 | Lemma
 | RTheorem
 | Corollary
 | Conjecture
 | NegatedConjecture
 | Plain
 | Type
 | RUnknown
 deriving(Eq)

instance Show Role where
  show role =
    case role of
     Axiom -> "axiom"
     Hypothesis ->"hypothesis"
     Assumption -> "assumption"
     Lemma -> "lemma"
     RTheorem -> "theorem"
     Corollary -> "corollary"
     Conjecture -> "conjecture"
     NegatedConjecture -> "negated_conjecture"
     Plain -> "plain"
     Type -> "type"
     RUnknown -> "unknown"

instance Read Role where
  readsPrec _  str =
    let roles = [Axiom,Hypothesis,Assumption,Lemma,RTheorem,Corollary,Conjecture,NegatedConjecture,Plain,Type,RUnknown]
    in
      map (\(ro,(s,r))-> (ro,r))
        $filter (\(ro,(s,r)) -> show ro == s)
          $ zip roles $ map (\role -> splitAt (length (show role)) str) roles

-- | They are accepted, without proof, as a basis for proving conjectures in THF, TFF, and FOF problems. In CNF problems the axiom-like formulae are accepted as part of the set whose satisfiability has to be established.
isAxiomLike :: Role -> Bool
isAxiomLike role =
  role `elem` [Axiom, Hypothesis, Definition, Assumption, Lemma, RTheorem, Corollary]

data Status =
   Theorem
 | ContradictoryAxioms
 | Satisfiable
 | Unsatisfiable
 | CounterSatisfiable
 | Unknown
 | Open
 deriving(Show,Read,Eq)


shouldBeTrue :: Status -> Bool
shouldBeTrue status =
  status `elem` [Theorem,ContradictoryAxioms]

shouldBeFalse :: Status -> Bool
shouldBeFalse status =
  status `elem` [CounterSatisfiable,Unsatisfiable]

shouldBeUnknown :: Status -> Bool
shouldBeUnknown status =
  status `elem` [Satisfiable,Unknown,Open]

statusToResult :: Status -> Result
statusToResult status
  | shouldBeTrue status = YES
  | shouldBeFalse status = NO
  | shouldBeUnknown status = UNKNOWN

data Result = YES | NO | UNKNOWN deriving (Show,Read,Eq,Enum,Bounded)

data Info
  = Info{
      language :: Maybe Language,
      status :: Maybe Status,
      filename :: String,
      result :: Result,
      usedMaxDepth :: Int,
      url :: String,
      strcontext :: String,
      strtarget :: String,
      strprocessed :: String,
      strnegated :: String,
      note :: String,
      context :: [DT.Preterm],
      suEnv :: [DT.Preterm],
      signature :: DT.Signature,
      prelst :: [(String,Int)],
      negated_conjecture :: Maybe DT.Preterm,
      target :: Maybe DT.Preterm,
      contextWithTexpr :: [F.Expr],
      targetWithTexpr :: Maybe F.Expr,
      negatedWithTexpr :: Maybe F.Expr
    } deriving (Show,Eq)

instance Default Info where
  def = Info{signature = [],suEnv = [],usedMaxDepth = 0,contextWithTexpr = [],targetWithTexpr=Nothing,negatedWithTexpr = Nothing,url = "",strnegated = "",negated_conjecture = Nothing,language = Nothing,status=Nothing,prelst=[],filename = "",result = UNKNOWN,strcontext = "",strtarget = "",strprocessed = "",note = "",context=[],target=Nothing}
