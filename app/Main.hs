import System.Console.CmdArgs
import System.Environment (getArgs)
import System.Directory
import System.Timeout
import Control.Monad
import qualified Data.Maybe
import Data.List.Split
import Data.Default (Default(..))
import qualified Debug.Trace as D
import qualified Data.Text.Lazy.IO as T
import Data.Time as TIME

import qualified Eval as E
import qualified TPTPInfo as TI
import qualified FileParser as F
import qualified TPTPSetting as TS

import qualified DTS.Alligator.ProofTree as APT
import qualified DTS.DTT as DT
import qualified DTS.Alligator.AlligatorBase as AB 

answerIs :: TI.Result -> DT.Preterm -> TI.Info -> TS.TestSetting -> IO TI.Info
answerIs yesOrNo conjecture base setting=
  let proofMode = case TS.mode setting of "plain" -> AB.Plain ; "efq" -> AB.WithEFQ ; _  -> AB.WithDNE
      result = APT.prove' (TI.suEnv base) (TI.signature base) conjecture AB.settingDef{AB.mode = proofMode}{AB.maxdepth = TS.depth setting}{AB.debug = "True" == TS.isDebug_ setting}
      resultTree = AB.trees result
      url = (TS.outputFolder setting)++takeWhile (/= '.') ( reverse$ takeWhile (/='/')$ reverse $init$TI.filename base) ++ "_"++ (TS.mode setting) ++".html"
  in
    if not (null resultTree)
    then do
      contents <- APT.announce' result
      T.writeFile url contents
      return $base {TI.result =  yesOrNo}{TI.url = url}{TI.usedMaxDepth = AB.usedMaxDepth $ AB.rStatus result}
    else
      return base

getResults :: TI.Info -> TS.TestSetting ->  IO TI.Info
getResults base setting =
  case TI.target base  of
    Nothing -> return base
    Just conjecture -> do
      let predictionIsYes = Data.Maybe.maybe False ((TI.YES==).TI.statusToResult) (TI.status base)
          predictionIsNo = Data.Maybe.maybe False ((TI.NO==).TI.statusToResult) (TI.status base)
      justYes <-  if predictionIsNo then return $Just base else timeout (TS.timeLimit setting) $ answerIs TI.YES conjecture base setting{TS.mode="plain"}
      let yes =  Data.Maybe.fromMaybe base{TI.note="timeout@yesPlain"} justYes
      yes' <- 
            if (TI.result yes /= TI.YES) && (TS.mode setting /= "plain") then do
              justYes' <-  if predictionIsNo then return $Just base else timeout (TS.timeLimit setting) $ answerIs TI.YES conjecture base setting
              return $Data.Maybe.fromMaybe base{TI.note ="timeout@yesDNE"} justYes'
            else return yes
      if TI.result yes' == TI.YES then do
        return yes'
      else do
        justNo <- if predictionIsYes then return justYes else timeout (TS.timeLimit setting) $ answerIs TI.NO (DT.Not conjecture) base setting{TS.mode="plain"}
        let no = Data.Maybe.fromMaybe base{TI.note = "timeout@noPlain"} justNo
        if (TI.result no /= TI.NO) && (TS.mode setting /= "plain") then do
          justNo' <-  if predictionIsNo then return justYes else timeout (TS.timeLimit setting) $ answerIs TI.NO (DT.Not conjecture) base setting
          return $Data.Maybe.fromMaybe base{TI.note = "timeout@noDNE"} justNo'
        else return no
        -- return $Data.Maybe.fromMaybe base justNo

csvHeader :: String
csvHeader="file\tassestment\tstatus\tresult\turl\tlanguage\tcontext\ttarget\tnote\ttime\tusedDepth\tsetDepth\tsetTime\n"

generateCsvRow :: TI.Info -> TIME.UTCTime -> TIME.UTCTime -> TS.TestSetting -> String
generateCsvRow info end start setting =
  TI.filename info ++ "\\t" ++
  (case TI.status info of Just sta -> show (TI.statusToResult sta ); _ -> "") ++ "\\t" ++
  (case TI.status info of Just sta -> show sta ; _ -> "") ++ "\\t" ++
  show (TS.mode setting) ++ "\\t"++
  show (TI.result info) ++ "\\t"++
  TI.url info ++ "\\t"++
  (case TI.language info of Just lan -> show lan ; _ -> "") ++ "\\t" ++
  TI.strcontext info ++ "\\t" ++
  TI.strtarget info ++ "\\t" ++
  TI.note info  ++"\\t" ++
  (show $TIME.diffUTCTime end start)  ++"\\t" ++
  (show $TI.usedMaxDepth info)  ++"\\t" ++
  (show $TS.depth setting)  ++"\\t" ++
  (show $TS.timeLimit setting)  

testInfoFile :: TS.TestSetting -> IO TI.Info
testInfoFile setting= do
  info <- F.fileparseInfo setting
  getResults info setting

main :: IO()
main = do
  setting <- cmdArgs TS.testSetting
  start <- TIME.getCurrentTime
  result <- testInfoFile setting
  end <- TIME.getCurrentTime
  putStrLn $generateCsvRow result end start setting
