module FileParser (fileparseInfo) where

import Eval (evalInfo)
import Parser (parseExpr)
import System.Environment

import qualified TPTPSetting as TS 

import TPTPInfo
import Data.Default (Default(..))

processInfo :: String -> TS.TestSetting -> IO Info
processInfo input setting = do
  let ast' = parseExpr input
  case ast' of
    Right ast -> evalInfo ast setting
    Left err ->
      return $ def {note = "Parser Error" ++ show err}


fileparseInfo :: TS.TestSetting -> IO Info
fileparseInfo setting  = do
  input <- readFile $TS.fname setting
  processInfo input setting
