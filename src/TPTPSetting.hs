{-# LANGUAGE DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-cse #-}
module TPTPSetting where
import System.Console.CmdArgs

data TestSetting
  = TestSetting{
    isDebug_ :: String ,       -- 計算中の出力の有無 (Boolの場合は$True)
    depth :: Int,           -- 後ろ向き推論の際の探索の深さ上限
    outputFolder :: FilePath, -- 証明木htmlの出力先
    timeLimit :: Int,       -- 推論全体を打ち切る時間(マイクロ秒)
    tptpdir :: FilePath,      -- TPTPライブラリの場所・import axiomで使う
    fname :: FilePath,        -- 問題ファイル
    timer :: String,          -- 計算時間の表示の有無 (Boolの場合は$True)
    mode :: String
  }deriving (Show, Data, Typeable)


testSetting = TestSetting
              {
                isDebug_ = "False"  &= help "計算中の出力の有無"
                ,depth = 8 &= help "後ろ向き推論の際の探索の深さ上限"
                ,outputFolder = "../../output/" &= help "証明木htmlの出力先"  &= typDir
                ,timeLimit = 100000000  &= help "推論全体を打ち切る時間(マイクロ秒)" &= name "l"
                ,tptpdir = "../../TPTP-v7.3.0/"  &= help "TPTPライブラリの場所・import axiomで使う" &= name  "t" &= typDir
                ,timer = "True" &= help "計算時間の表示の有無" &= name "s"
                ,fname = def  &= typ "FILES/DIRS" &= argPos 0
                ,mode = "dne" &= help "証明モード設定(dne/efq/plain)" &= name "m"
              }
              &= summary "setting Sample"
              &= program "TPTPTEST"
